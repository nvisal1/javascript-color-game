let numberOfTiles = 6;
let colors = [];
let winCondition;
const tiles = document.querySelectorAll('.square');
const rgbText = document.querySelector('#rgbText');
const message = document.querySelector('#message');
const reset = document.querySelector('#new');
const modeButtons = document.querySelectorAll('.mode');
const header = document.querySelector('#header');

function generateRandomRGBValue() {
  const red = Math.floor((Math.random() * 256));
  const green = Math.floor((Math.random() * 256));
  const blue = Math.floor((Math.random() * 256));
  return 'rgb('+ red + ', ' + green + ', ' + blue + ')';
}

function generateRandomColors(num) {
  const colorArray = [];
  for (let x = 0; x < num; x++) {
    colorArray.push(generateRandomRGBValue());
    console.log(colorArray[x]);
  }
  return colorArray;
}

function pickWinCondition() {
  const index = Math.floor((Math.random() * colors.length));
  return colors[index];
}

function displayWinningColor() {
  for (let x = 0; x < colors.length; x++) {
    tiles[x].style.backgroundColor = winCondition;
  }
  header.style.backgroundColor = winCondition;
}

function setUpModeButtonListeners() {
  for (let i = 0; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener('click', function () {
      modeButtons[0].classList.remove('selected');
      modeButtons[1].classList.remove('selected');
      this.classList.add('selected');
      this.textContent === 'Easy' ? numberOfTiles = 3 : numberOfTiles = 6;
      resetGame();
    });
  }
}

function setUpTileEventListeners() {
  for (let i = 0; i < tiles.length; i++) {
    // add click listener to each tile
    tiles[i].addEventListener('click', function () {
      const selectedColor = this.style.backgroundColor;
      if (selectedColor === winCondition) {
        reset.textContent = 'Play Again?';
        message.textContent = 'Correct!';
        displayWinningColor();
      }
      else {
        message.textContent = 'Try Again!';
        // Tile vanishes when incorrect is chosen
        this.style.backgroundColor = '#232323';
      }
    });
  }
}

function resetGame() {
  reset.textContent = 'New Colors';
  message.textContent = '';
  colors = generateRandomColors(numberOfTiles);
  winCondition = pickWinCondition();

  for (let i = 0; i < tiles.length; i++) {
    // set colors to tiles
    if (colors[i]) {
      tiles[i].style.display = 'block';
      tiles[i].style.backgroundColor = colors[i];
    } else {
      tiles[i].style.display = 'none';
    }
  }
  rgbText.textContent = winCondition;
  header.style.backgroundColor = '#4353ff';
}

reset.addEventListener('click', function () {
  resetGame();
});

function init() {
  // Mode buttons (Easy and Hard) event lsteners
  setUpModeButtonListeners();
  // Tile event listeners
  setUpTileEventListeners();
  resetGame();
}

init();
